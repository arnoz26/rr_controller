

class Encoder
{
public:
    uint8_t pinA = -1;
    uint8_t pinB = -1;
    uint8_t pinAidx = -1;
    uint8_t pinBidx = -1;
    uint8_t increment = 20;
    volatile uint16_t position = 511; // cumulative changes for use as an axis
    uint16_t prevPosition = 511;
    uint8_t state = 0; // bits: a, b, prevA, prevB, isAxis, interruptPinA, buttonStateLeft, buttonStateRight
    uint8_t leftAssignment = 0;
    uint8_t rightAssignment = 0;
    uint8_t axisAssignment = 0;

    uint8_t holdTime = 100; //milliseconds before left/right button released

    bool leftDown = false;
    bool rightDown = false;
    long leftPushedTime = 0;
    long rightPushedTime = 0;

    long lastChange = 0;

    Encoder()
    {
    }

    void ToDefault(){
        pinA = -1;
        pinB = -1;
        pinAidx = -1;
        pinBidx = -1;
        leftAssignment = 0;
        rightAssignment = 0;
    }

    void SetPins(uint8_t aIdx, uint8_t bIdx, uint8_t A, uint8_t B)
    {
        pinA = A;
        pinB = B;
        pinAidx = aIdx;
        pinBidx = bIdx;
        pinMode(pinA, INPUT_PULLUP);
        pinMode(pinB, INPUT_PULLUP);

        SetIsAxis(true);
    }

    void Set(int property, int value)
    {
        switch (property)
        {
        case 1:
            pinAidx = value;
            break;
        case 2:
            pinBidx = value;
            break;
        case 3:
            pinA = value;
            pinMode(pinA, INPUT_PULLUP);
            break;
        case 4:
            pinB = value;
            pinMode(pinB, INPUT_PULLUP);
            break;
        case 5:
            leftAssignment = value;
            break;
        case 6:
            rightAssignment = value;
            break;
        }
    }

    // informs class whether to sample pins on update or
    // rely on interrupt
    void EnableInterruptA(bool isEnabled)
    {
        bitWrite(state, 5, isEnabled);
    }

    void onInterruptA()
    {
        if (millis() - lastChange > 10)
        {
            if (digitalRead(pinB))
            {
                position -= increment;
            }
            else
            {
                position += increment;
            }
            lastChange = millis();
        }
    }

    bool InterruptAEnabled()
    {
        return bitRead(state, 5);
    }

    // Set as an axis rather than left<>right buttons
    void SetIsAxis(bool isAxis)
    {
        bitWrite(state, 4, isAxis);
    }

    bool IsAxis()
    {
        return bitRead(state, 4);
    }

    int GetVal()
    {
        return position;
    }

    int GetVal(int direction)
    {
        if (IsAxis())
        {
            return position;
        }
        else
        {
            if (direction == 0)
            {
                return bitRead(state, 6);
            }
            else
            {
                return bitRead(state, 7);
            }
        }
    }

    void SetDigitalVal(int btnDir)
    {
        if (btnDir == 0)
        {
            bitWrite(state, 6, 1);
        }
        else
        {
            bitWrite(state, 7, 1);
        }
    }

    bool ButtonADown()
    {
        if (bitRead(state, 6))
        {
            bitWrite(state, 6, 0);
            leftPushedTime = millis();
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ButtonBDown()
    {
        
        if (bitRead(state, 7))
        {
            bitWrite(state, 7, 0);
            rightPushedTime = millis();
            return true;
        }
        else
        {
            return false;
        }
    }

    void ReportConfig()
    {
        Serial.write(pinAidx);
        Serial.write(pinBidx);
        Serial.write(leftAssignment);
        Serial.write(rightAssignment);

        // Placeholders for extra data later (absolute/analog mode)
        Serial.write(pinA);
        Serial.write(pinB);
        Serial.write(false);
        Serial.write(false);
        Serial.write(false);
        Serial.write(false);
    }

    void WriteToEEPROM(int memoryLocation, int idx)
    {
        idx = memoryLocation + (idx * 12);

        Store8BitValue(idx + 0, pinA);
        Store8BitValue(idx + 1, pinB);

        Store8BitValue(idx + 2, pinAidx);
        Store8BitValue(idx + 3, pinBidx);
        Store8BitValue(idx + 4, leftAssignment);
        Store8BitValue(idx + 5, rightAssignment);
    }

    void ReadFromEEPROM(int memoryLocation, int idx){
        idx = memoryLocation + (idx * 12);
        pinA = Read8BitValue(idx + 0);
        pinB = Read8BitValue(idx + 1);
        pinAidx = Read8BitValue(idx + 2);
        pinBidx = Read8BitValue(idx + 3);
        leftAssignment = Read8BitValue(idx + 4);
        rightAssignment = Read8BitValue(idx + 5);

        SetPins(pinAidx, pinBidx, pinA, pinB);
    }

    void UpdateInput()
    {
        
        //     Serial.print(leftDown); 
        //     Serial.print(rightDown);            
        
        // Serial.println();

        if (leftDown && millis() - leftPushedTime > 100){
            leftDown = false;
        }
        if (rightDown && millis() - rightPushedTime > 100){
            rightDown = false;
        }

        // Poll update if interrupt is no enabled
        if (!InterruptAEnabled())
        {

            // Store current state
            bitWrite(state, 0, digitalRead(pinA));
            bitWrite(state, 1, digitalRead(pinB));

            // if A is low and was previously high
            if (bitRead(state, 0) == 0 && bitRead(state, 2) == 1)
            {
                if (bitRead(state, 1) == 0)
                {
                    position -= increment;
                    bitWrite(state, 6, 1);
                    leftDown = true;
                    leftPushedTime = millis();
                }
                else
                {
                    position += increment;
                    bitWrite(state, 7, 1);
                    //Serial.println("R");
                    rightDown = true;
                    rightPushedTime = millis();
                }
            }

            // Set current vals to prev for next update;
            bitWrite(state, 2, bitRead(state, 0));
            bitWrite(state, 3, bitRead(state, 1));
        }

        if (IsAxis())
        {
            if (position > 60000)
                position = 0;
            if (position > 1023)
                position = 1023;
        }
        prevPosition = position;
    }

    uint16_t GetPosition()
    {
        return position;
    }
};