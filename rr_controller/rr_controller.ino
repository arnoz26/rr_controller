// #include <EEPROM.h>
#include "InputClass.h"
#include "encoders.h"

// DON'T CHANGE
int i2c_address = 0;                    //0=stickbase, 1=throttlebase, 2=stick, 3=throttlestick
char deviceName[] = "NEW_DEVICE      "; //16 chars
int firmwareVersion = 7;                //beta

long loopStart = 0;
int loopDelay = 1000 / 150; // second number is loops per second

long lastUpdate = 0;
long lastSerialUpdate = 0;
long serialUpdateDelay = 1000 / 30;

void setup()
{
    InitComms();
    InitInputs();
    InitI2C();

    if (IsMaster())
    {
        Serial.print("known subdevices: ");
        Serial.println(SubDeviceCount());
    }
}

void loop()
{
    //Serial.println(millis() - loopStart);

    // loopStart = millis();
    CheckComms();
    UpdateInputs();

    if (IsMaster())
    {
        GetSubDeviceInputs();
    }

    //
    //    for (int i = 0; i < 16; i++){
    //      Serial.print(GetInput(i).GetPin());
    //      Serial.print('\t');
    //    }
    //    Serial.println();

    if (IsConnectedToPC())
    {
        if (millis() - lastSerialUpdate > serialUpdateDelay)
        {
            SendInputValuesToSerial();
            lastSerialUpdate = millis();
        }
    }
    delay(5);
}
